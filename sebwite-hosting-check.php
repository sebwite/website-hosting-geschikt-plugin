<?php
/*
Plugin Name: Sebwite Hosting Geschikt
Plugin URI: http://sebwite.nl/
Description: Voegt een admin pagina toe voor sebadmin users waar we kijken of deze hosting geschikt is
Author: Sebwite
Author URI: http://sebwite.nl/
Text Domain: sebwite-hosting-geschikt
Version: 0.12

Bitbucket Plugin URI: 	https://bitbucket.org/sebwite/website-hosting-geschikt-plugin
Bitbucket Branch:		master

*/

define( 'PLUGIN_SUPERADMIN', 'sebadmin' );

/** Step 2 (from text above). */
add_action( 'admin_menu', 'shg_plugin_menu' );

/** Step 1. */
function shg_plugin_menu() {

    $current_user = wp_get_current_user();

    if( $current_user->user_login == PLUGIN_SUPERADMIN ) {
        add_submenu_page(
            'options-general.php' 
            , __('Hosting Geschikt','sebwite-hosting-geschikt')
            , __('Hosting Geschikt','sebwite-hosting-geschikt') 
            , 'manage_options'
            , 'sebwite-hosting-geschikt'
            , 'shg_main_page'
         );        
    }
}

/** Step 3. */
function shg_main_page() {
	$current_user = wp_get_current_user();
    if ( $current_user->user_login != PLUGIN_SUPERADMIN )  {

        wp_die( __( 'U heeft niet genoeg rechten om deze pagina te bekijken.','sebwite-hosting-geschikt' ) );
		
    } else {
   	include( plugin_dir_path( __FILE__ ) .  'script/index.php');
	}
}

function shg_verify_current_user() {

}

function hosting_check_textdomain() {
	// Localization
	load_plugin_textdomain('sebwite-hosting-geschikt', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}


