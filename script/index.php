<?php
// Database variabelen
$db_host = 'localhost';
$db_user = 'productie';
$db_password = 'sebhq123';

/**
 * Vraag een php.ini waarde op en strip alle karakters behalve cijfers zodat we ermee kunnen rekenen
 */
function get_php_ini_value_raw_plugin( $value ) {

    $php_value = ini_get( $value );
    $raw_value = preg_replace("/[^0-9,.]/", "", $php_value);

    return $raw_value;
}

/**
 * Boolean naar string
 */
function ini_boolean_to_value( $boolean ) {
    if( $boolean )
        return 'On';
    else
        return 'Off';
}

/**
 * Kijk of we ini_set kunnen
 */
function ini_set_usuable() {

    $current_memory_limit = (int) get_php_ini_value_raw_plugin( 'memory_limit' );

    $try_memory_limit = $current_memory_limit + 1;
    ini_set( 'memory_limit', $try_memory_limit );

    // Kijk of het memory_limit kunnen wijzigen
    if( $current_memory_limit != (int) get_php_ini_value_raw_plugin( 'memory_limit' ) ) {

        // memory_limit is gewijzigd dus we zetten het weer terug en geven aan dat we ini_set kunne gebruiken
        ini_set( 'memory_limit', $current_memory_limit );

        return true;
    }

    return false;
}

/**
 * Haal de MySQL versie op
 */
function get_mysql_version( $db_host, $db_user, $db_password ) {

    $link = mysql_connect( $db_host, $db_user, $db_password );
    if (!$link) {
    	return 'Kan niet verbinden met MySQL';
    }

    return mysql_get_server_info();
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans|Open+Sans' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Sebwite - Hosting Geschikt</title>
    <style type="text/css" media="screen">
        body {
           
            min-width: 0;
            color: #444;
            font-family: "Open Sans",sans-serif;
            font-size: 13px;
            line-height: 1.4em;
        }

        .header-image {
            margin: 0 auto 20px;
            display: block;
        }

        h1, h2, h3 {
            text-align: center;
            font-family: 'Droid Sans', sans-serif;
        }

        .container {
            width: 800px;
            margin: 20px auto 0;
        }

        table.info {
            width: 100%;
            margin-bottom: 20px;
            padding: 0 10px 10px;

            background: #fff;
            -webkit-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.5);
            box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.5);
        }

        th {
            text-align: left;
        }

        tbody tr td {
            border-top: 1px solid #f1f1f1 !important;
        }

        .description {
            display: none;
        }

        .footer {
            text-align: center;
        }
    </style>
</head>

<?php
	if (!function_exists('is_plugin_active')) { 
		/* ========================================================
		 *  VOEG HIER STYLING TOE ALS HET GEEN WORDPRESS PLUGIN IS 
		 * ========================================================*/
	?>
	<style>
		html {
			background-color: #f1f1f1;
		}
	</style>
	<?php
	}
?>


<div class="container">
	<?php
	if (function_exists('is_plugin_active')) {
		if ( is_plugin_active( 'website-hosting-geschikt-plugin/sebwite-hosting-check.php' ) ) { 
		?>
	  	 <img src="<?php echo plugins_url( 'assets/images/logo-sebwite.png' , __FILE__ )?>" class="header-image">
		<?php
		} 
	}	else { ?>
			<img src="assets/images/logo-sebwite.png" class="header-image">
		<?php	}
	?>
   

    <h1>Sebwite - Hosting Geschikt</h1>

    <?php
    /**
     * ---------------------------------------------------------------------------------------------
     *
     * Software
     *
     * ---------------------------------------------------------------------------------------------
     */
    ?>

    <table class="info">
        <thead>
        <td colspan="3"><h2>Software</h2></td>
        </thead>
        <tr>
            <th>Software</th>
            <th>Versie</th>
            <th>Aanbevolen versie</th>
        </tr>

        <!-- PHP Versie -->
        <?php
        $color = 'red';

        if (version_compare(phpversion(), '5.3', '>='))
            $color = 'green';
        ?>
        <tr>
            <td>PHP versie</td>
            <td style="color: <?php echo $color; ?>"><?php echo phpversion(); ?></td>
            <td>5.3 of hoger</td>
        </tr>

        <!-- MySQL Versie -->
        <?php
        $color = 'red';

        if( version_compare( get_mysql_version( $db_host, $db_user, $db_password ), '5.0.0', '>=' ) )
            $color = 'green';

        ?>
        <tr>
            <td>MySQL versie</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_mysql_version( $db_host, $db_user, $db_password ); ?></td>
            <td>5.0.0 of hoger</td>
        </tr>
    </table>

    <table class="info">
        <thead>
        <td colspan="3"><h2>Informatie</h2></td>
        </thead>
        <tr>
            <th>Variabele</th>
            <th>Waarde</th>
        </tr>

        <!-- __DIR__ -->
        <tr>
            <td>__DIR__</td>
            <td><?php echo __DIR__; ?></td>
        </tr>

        <!-- __FILE__ -->
        <tr>
            <td>__FILE__</td>
            <td><?php echo __FILE__; ?></td>
        </tr>
    </table>

    <table class="info">
        <thead>
            <td colspan="3"><h2>PHP waarden</h2></td>
        </thead>
        <tr>
            <th>Variabele</th>
            <th>Huidige waarde</th>
            <th>Aanbevolen waarde</th>
        </tr>

        <!-- PHP - upload_max_filesize -->
        <?php
        $color = 'red';

        if( (int) get_php_ini_value_raw_plugin( 'upload_max_filesize' ) >= 32 )
            $color = 'green';
        ?>
        <tr>
            <td>upload_max_filesize</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_php_ini_value_raw_plugin( 'upload_max_filesize' ); ?>M</td>
            <td>32M</td>
        </tr>

        <!-- PHP - post_max_size -->
        <?php
        $color = 'red';

        if( (int) get_php_ini_value_raw_plugin( 'post_max_size' ) >= 32 )
            $color = 'green';
        ?>
        <tr>
            <td>post_max_size</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_php_ini_value_raw_plugin( 'post_max_size' ); ?>M</td>
            <td>32M</td>
        </tr>

        <!-- PHP - max_input_vars -->
        <?php
        $color = 'red';

        if( (int) get_php_ini_value_raw_plugin( 'max_input_vars' ) >= 5000 )
            $color = 'green';
        ?>
        <tr>
            <td>max_input_vars</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_php_ini_value_raw_plugin( 'max_input_vars' ); ?></td>
            <td>5000</td>
        </tr>

        <!-- PHP - memory_limit -->
        <?php
        $color = 'red';

        if( (int) get_php_ini_value_raw_plugin( 'memory_limit' ) >= 128 )
            $color = 'green';
        ?>
        <tr>
            <td>memory_limit</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_php_ini_value_raw_plugin( 'memory_limit' ); ?>M</td>
            <td>128M</td>
        </tr>

        <!-- PHP - memory_limit -->
        <?php
        $color = 'red';

        if( (int) get_php_ini_value_raw_plugin( 'max_execution_time' ) < 128 )
            $color = 'green';
        ?>
        <tr>
            <td>max_execution_time</td>
            <td style="color: <?php echo $color; ?>"><?php echo get_php_ini_value_raw_plugin( 'max_execution_time' ); ?></td>
            <td>60</td>
        </tr>
    </table>

    <?php
    /**
     * ---------------------------------------------------------------------------------------------
     *
     * Optionele PHP waarden ivm. veiligheid
     *
     * ---------------------------------------------------------------------------------------------
     */
    ?>

    <table class="info">
        <thead>
        <td colspan="3"><h2>Optionele PHP waarden ivm. veiligheid</h2></td>
        </thead>
        <tr>
            <th>Variabele</th>
            <th>Huidige waarde</th>
            <th>Aanbevolen waarde</th>
        </tr>

        <!-- PHP - allow_url_fopen -->
        <?php
        $color = 'red';

        if( ini_get( 'allow_url_fopen' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>allow_url_fopen</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'allow_url_fopen' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">If enabled, allow_url_fopen allows PHP's file functions -- such as file_get_contents() and the include and require statements -- can retrieve data from remote locations, like an FTP or web site. Programmers frequently forget this and don't do proper input filtering when passing user-provided data to these functions, opening them up to code injection vulnerabilities.</td>
        </tr>

        <!-- PHP - allow_url_include -->
        <?php
        $color = 'red';

        if( ini_get( 'allow_url_include' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>allow_url_include</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'allow_url_include' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">If disabled, allow_url_include denies remote file access via the include and require statements, but leaves it available for other file functions like fopen() and file_get_contents. include and require are the most common attack points for code injection attempts, so this setting plugs that particular hole without affecting the remote file access capabilities of the standard file functions.</td>
        </tr>

        <!-- PHP - disable_functions -->
        <?php
        $color = 'red';

        if( ini_get( 'disable_functions' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>disable_functions</td>
            <td style="color: <?php echo $color; ?>"><?php //echo ini_get( 'disable_functions' ); ?></td>
            <td><!--"exec,passthru,shell_exec,system,proc_open,curl_multi_exec,parse_ini_file,show_source"--></td>
        </tr>
        <tr>
            <td class="description" colspan="3">Disabling dangerous functions helps make sure they are not used by the application and prevents attackers from using them. Most dangerous functions allow executing external system commands - this functionality is inherently dangerous, because it may allow command injection vulnerabilities if not implemented correctly.</td>
        </tr>

        <!-- PHP - display_errors -->
        <?php
        $color = 'red';

        if( ini_get( 'display_errors' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>display_errors</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'display_errors' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">The display_errors directive determines whether error messages should be sent to the browser. These messages frequently contain sensitive information about your web application environment, and should never be presented to untrusted sources.</td>
        </tr>

        <!-- PHP - error_reporting -->
        <?php
        $color = 'red';

        if( ini_get( 'error_reporting' ) == -1 )
            $color = 'green';
        ?>
        <tr>
            <td>error_reporting</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_get( 'error_reporting' ); ?></td>
            <td>-1</td>
        </tr>
        <tr>
            <td class="description" colspan="3">Reports any and all errors that occur on the server.</td>
        </tr>

        <!-- PHP - expose_php -->
        <?php
        $color = 'red';

        if( ini_get( 'expose_php' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>expose_php</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'expose_php' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">Enabled by default, expose_php reports in every request that PHP is being used to process the request, and what version of PHP is installed. Malicious users looking for potentially vulnerable targets can use this to identify a weakness.</td>
        </tr>

        <!-- PHP - html_errors -->
        <?php
        $color = 'red';

        if( ini_get( 'html_errors' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>html_errors</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'html_errors' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">PHP has the capability of inserting html links to documentation related to an error. This directive controls whether those HTML links appear in error messages or not. For performance and security reasons, it's recommended you disable this on production servers.</td>
        </tr>

        <!-- PHP - log_errors -->
        <?php
        $color = 'red';

        if( ini_get( 'log_errors' ) == true )
            $color = 'green';
        ?>
        <tr>
            <td>log_errors</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'log_errors' ) ); ?></td>
            <td>On</td>
        </tr>
        <tr>
            <td class="description" colspan="3">Logging Server issues is vital for good security.</td>
        </tr>

        <!-- PHP - magic_quotes_gpc -->
        <?php
        $color = 'red';

        if( ini_get( 'magic_quotes_gpc' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>magic_quotes_gpc</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'magic_quotes_gpc' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">The magic quotes option was introduced to help protect against SQL injection attacks. It effectively executes addslashes() on all information received over GET, POST or COOKIE. Unfortunately this protection isn't perfect: there are a series of other characters that databases interpret as special not covered by this function. In addition, data not sent direct to databases must un-escaped before it can be used.</td>
        </tr>

        <!-- PHP - open_basedir -->
        <?php
        $color = 'red';

        if( ini_get( 'open_basedir' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>open_basedir</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_get( 'open_basedir' ); ?></td>
            <td><!--Off--></td>
        </tr>
        <tr>
            <td class="description" colspan="3">open_basedir limits the PHP process from accessing files outside of specifically designated directories. Remember this setting will only affect PHP scripts. Applications written in other languages (Perl, Python, Ruby, etc.) will not be affected. NOTE: You will need to change the value of <YOURUSERNAME> to your actual username on your server.</td>
        </tr>

        <!-- PHP - register_globals -->
        <?php
        $color = 'red';

        if( ini_get( 'register_globals' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>register_globals</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'register_globals' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">When register_globals is enabled, PHP will automatically create variables in the global scope for any value passed in GET, POST or COOKIE. This, combined with the use of variables without initialization, has lead to numerous security vulnerabilities.</td>
        </tr>

        <!-- PHP - safe_mode -->
        <?php
        $color = 'red';

        if( ini_get( 'safe_mode' ) == false )
            $color = 'green';
        ?>
        <tr>
            <td>safe_mode</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'safe_mode' ) ); ?></td>
            <td>Off</td>
        </tr>
        <tr>
            <td class="description" colspan="3">PHP safe mode is an attempt to solve the shared-server security problem. It is architecturally incorrect to try to solve this problem at the PHP level, and although it was popular years ago with ISPs, few consider this an effective solution currently.</td>
        </tr>

        <!-- PHP - upload_tmp_dir -->
        <?php
        $color = 'red';

        if( ini_get( 'upload_tmp_dir' ) == true )
            $color = 'green';
        ?>
        <tr>
            <td>upload_tmp_dir</td>
            <td style="color: <?php echo $color; ?>"><?php echo ini_boolean_to_value( ini_get( 'upload_tmp_dir' ) ); ?></td>
            <td>On</td>
        </tr>
        <tr>
            <td class="description" colspan="3">The temporary directory is used for storing files when a file is uploaded.</td>
        </tr>

    </table>

    <?php
    $can_get_modules = false;
    $apache_modules = array();

    if( function_exists('apache_get_modules') ) {
        $can_get_modules = true;
    }

    if( $can_get_modules ) {
        $apache_modules = apache_get_modules();
    }
    ?>

    <table class="info">
        <thead>
        <td colspan="3"><h2>Apache modules</h2></td>
        </thead>

        <?php if( $can_get_modules ) : ?>
            <tr>
                <th>Module</th>
                <th>Huidige waarde</th>
                <th>Aanbevolen waarde</th>
            </tr>

            <!-- Apache - mod_alias -->
            <?php
            $apache_module = 'mod_alias';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_authz_host -->
            <?php
            $apache_module = 'mod_authz_host';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_deflate -->
            <?php
            $apache_module = 'mod_deflate';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_dir -->
            <?php
            $apache_module = 'mod_dir';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_expires -->
            <?php
            $apache_module = 'mod_expires';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_headers -->
            <?php
            $apache_module = 'mod_headers';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_mime -->
            <?php
            $apache_module = 'mod_mime';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_rewrite -->
            <?php
            $apache_module = 'mod_rewrite';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_log_config -->
            <?php
            $apache_module = 'mod_log_config';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_autoindex -->
            <?php
            $apache_module = 'mod_autoindex';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_negotiation -->
            <?php
            $apache_module = 'mod_negotiation';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>

            <!-- Apache - mod_setenvif -->
            <?php
            $apache_module = 'mod_setenvif';

            $color = 'red';
            $response = '';

            if( in_array( $apache_module, apache_get_modules() ) ) {
                $color = 'green';
                $response = 'On';
            }
            else {
                $color = 'red';
                $response = 'Off';
            }
            ?>
            <tr>
                <td><?php echo $apache_module; ?></td>
                <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
                <td>On</td>
            </tr>
        <?php else : ?>
            <tr>
                <td style="color: orange; text-align: center">Kan Apache modules niet uitlezen. Raadpleeg <a href="phpinfo.php">phpinfo</a>.</td>
            </tr>
        <?php endif; ?>
    </table>


    <?php
    /**
     * ---------------------------------------------------------------------------------------------
     *
     * Woocommerce aanbevelingen
     *
     * ---------------------------------------------------------------------------------------------
     */
    ?>

    <table class="info">
        <thead>
            <td colspan="3"><h2>Woocommerce aanbevelingen</h2></td>
        </thead>
        <tr>
            <th>Module/Software</th>
            <th>Huidig</th>
            <th>Aanbevolen</th>
        </tr>

        <!-- fsockopen & cURL -->
        <?php
        $color = 'red';
        $response = '';

        if( function_exists( 'fsockopen' ) || function_exists( 'curl_init' ) ) {

            if( function_exists( 'fsockopen' ) && function_exists( 'curl_init' ) ) {
                $response = 'Beide fsockopen en cURL staan aan';
            }
            elseif ( function_exists( 'fsockopen' )) {
                $response = 'Alleen fsockopen staan aan - cURL staat uit';
            }
            else {
                $response = 'Alleen cURL staan aan - fsockopen staat uit';
            }
            $color = 'green';
        }
        else {
            $response = 'Beide fsockopen en cURL staan niet aan - PayPal IPN en andere scripts om te communiceren met andere servers werken niet.';
            $color = 'red';
        }
        ?>
        <tr>
            <td>fsockopen/cURL</td>
            <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
            <td>fsockopen en/of cURL</td>
        </tr>

        <!-- SOAP CLient -->
        <?php
        $color = 'red';
        $response = '';

        if ( class_exists( 'SoapClient' ) ) {
            $response = 'De SOAP Client class is toegankelijk';
            $color = 'green';
        } else {
            $response = 'De SOAP Client class is niet toegankelijk - enkele gateway plugins die SOAP gebruiken kunnen niet werken zoals verwacht';
            $color = 'red';
        }
        ?>
        <tr>
            <td>SOAP Client</td>
            <td style="color: <?php echo $color; ?>"><?php echo $response; ?></td>
            <td>SOAP Client toegankelijk</td>
        </tr>

    </table>

    <?php
    /**
     * ---------------------------------------------------------------------------------------------
     *
     * Overige info
     *
     * ---------------------------------------------------------------------------------------------
     */
    ?>

    <table class="info">
        <thead>
            <td><h2>Overige info</h2></td>
        </thead>
        <tr>
            <th>Informatie</th>
        </tr>

        <!-- PHP - memory_limit -->
        <?php
        $color = 'red';

        if( ini_set_usuable() )
            $color = 'green';
        ?>
        <tr>
            <td style="color: <?php echo $color; ?>"><?php echo ini_set_usuable() ? 'We kunnen ini_set() gebruiken om php.ini waarden aan te passen in runtime' : 'We kunnen ini_set() <b>niet</b> gebruiken'; ?></td>
        </tr>
    </table>

    <footer class="footer">
        Hosting Geschikt door Sebwite
    </footer>

    <?php //phpinfo(); ?>
</div>
</html>

<?php ?>