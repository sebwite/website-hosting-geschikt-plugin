# README #

### Wat doet deze plugin? ###

* Deze plugin bekijkt of de hosting waarop wordpress is geinstalleerd

### Hoe gebruik ik deze plugin ###

https://wiki.sebwite.nl/article/hosting-geschikt/

* Download de laatste tag. 
* Installeer de plugin in Wordpress door hem te uploaden via Wordpress.
* Activeer de plugin in WordPress.
* Bezoek de plugin bij 'Instellingen -> Hosting geschikt'

